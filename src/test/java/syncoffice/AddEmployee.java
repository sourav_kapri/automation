package syncoffice;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.awt.*;
import java.sql.Driver;
import java.util.concurrent.TimeUnit;

public class AddEmployee {
    WebDriver driver;
    @When("browser is open")
    public void browser_is_open() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.navigate().to("https://www.syncoffice.com/module-test-website/login");


    }

    @And("user enters username and password")
    public void user_enters_username_and_password() {
        System.out.println("Enter user name and p/w");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[1]/div/div[2]/div/div/div/form/div[1]/div[1]/div/input")).sendKeys("skapri04@gmail.com");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[1]/div/div[2]/div/div/div/form/div[2]/div[1]/div/input")).sendKeys("abc123");

    }

    @When("user click on login")
    public void user_click_on_login() {
        System.out.println(" on clicking login btn ");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[1]/div/div[2]/div/div/div/div[1]/div[2]/div[2]/button/span[1]")).click();
    }

    @Then("should open home page")
    public void should_open_home_page() {
        System.out.println(" display home page");

    }

    @And("should display welcome")
    public void should_display_welcome() {
        System.out.println("should display welcome");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[3]/div/div/div[2]/div[2]/div/div[1]")).isDisplayed();

    }

    @Then("user click on password manager")
    public void user_click_on_password_manager() {
       System.out.println("click on password manager");
       driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[1]/div[3]/div/div/div[2]/div[2]/div/div[2]/div[5]/a/div")).click();
    }
    @Then("should display password manager page")
    public void should_display_password_manager_page() {

    }

    @And("goto my password card and on clicking add icon")
    public void goto_my_password_card_and_on_clicking_add_icon() {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[1]/div[2]/div[2]/div/div[1]/div/div[1]/div/div/div[2]/div/h5/div[1]/div/button/span[1]/span")).click();

    }

    @Then("should open Add category drawer")
    public void should_open_Add_category_drawer() {
        System.out.println("should open Add category drawer");

    }

    @And("fill all the mandatory field And should enable save  button")
    public  void fill_all_the_mandatory_field_And_should_enable_save_button() {
        driver.findElement(By.xpath("/html/body/div[3]/div[3]/div/div/div[1]/div/div[1]/div[2]/div/div/div/div/div[1]/div/div/div[1]/div/input")).sendKeys("Category06");
        driver.findElement(By.xpath("/html/body/div[3]/div[3]/div/div/div[1]/div/div[1]/div[2]/div/div/div/div/div[4]/div/div/div[1]/div[1]/div[2]/div/div[2]/div")).click();
//        Select region = new Select(driver.findElement(By.xpath("/html/body/div[3]/div[3]/div/div/div[1]/div/div[1]/div[2]/div/div/div/div/div[4]/div/div/div[1]/div[1]/div[2]/input")));
//        region.selectByIndex(1);
        driver.findElement(By.xpath("/html/body/div[3]/div[3]/div/div/div[1]/div/div[1]/div[2]/div/div/div/div/div[4]/div/div/div[1]/div[1]/div[2]/input")).click();

       }

    @Then("on clicking save button data should save")
    public void on_clicking_save_button_data_should_save() {

    }


}


