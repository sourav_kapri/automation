@thread1
  Feature: Test add Employee functionality
    Scenario: add employee in a group company
      When browser is open
      And user enters username and password
      When user click on login
      Then should open home page
      And should display welcome
      Then user click on password manager
      And should display password manager page
      Then goto my password card and on clicking add icon
      And should open Add category drawer
      Then fill all the mandatory field And should enable save  button
      And on clicking save button data should save


